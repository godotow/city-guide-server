require "sinatra/base"
require "sinatra/reloader"

class Marker
  def self.all
    MarkerRepository.new.find_all
  end
end

class MarkerRepository
  INDEX = 'markers'
  def find_all distance='600m'
    search filtered: {
             query: { match_all: {} },
             filter: {
               geo_distance: { distance: distance, location: [21.015256, 52.24800230000001] }
             }
           }
  end

  private
  def search query
    # delete_index rescue nil
    # create_index
    # populate_documents
    client.search index: INDEX, body: {query: query}
  end

  def client
    Elasticsearch::Client.new log: true
  end

  def create_index
    client.indices.create index: INDEX, type: 'marker', body: {
                            mappings: {
                              marker: {
                                properties: {
                                  location: {
                                    type: "geo_point"
                                  }
                                }
                              }
                            }
                          }
  end

  def populate_documents
    json = JSON.load File.open('history.json').read
    json['features'].each do |doc|
      doc['location'] = doc['geometry']['coordinates'][0,2]
      doc.delete 'geometry'

      client.index index: INDEX, type: 'marker', body: doc
    end
  end

  def delete_index
    client.indices.delete index: INDEX
  end
end

class CityGuideServer < Sinatra::Base
  configure :development do
    register Sinatra::Reloader
  end

  get '/' do
  end

  get '/markers' do
    Marker.all['hits']['hits'].map{|h| h['_source']}.to_json
  end
end
