class CompoundTrace
  attr_reader :collection
  def initialize start
    @collection = [start]
  end

  def distance
    collection.inject(0){|sum,path| sum + path.distance}
  end

  def include? other
    collection.first.start == other || collection.last.stop == other
  end

  def start_at? point
    collection.first.start == point
  end

  def stop
    collection.last.stop
  end

  def add other
    collection.concat other.collection
    self
  end

  def ==(other)
    self.collection == other.collection
  end
end
