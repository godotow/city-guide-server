require 'simple_trace'
require 'compound_trace'

class Trace
  attr_reader :path
  def initialize start, stop, distance
    @path = CompoundTrace.new SimpleTrace.new(start,stop,distance)
  end

  def include? point
    path.include? point
  end

  def start_at? point
    path.start_at? point
  end

  def add(other)
    path.add other.path
  end

  def distance
    path.distance
  end

  def stop
    path.stop
  end

  def eql?(other)
    self == other
  end

  def ==(other)
    self.path == other.path
  end

  def inspect
    "<#{path.collection.map{|p|[p.start,p.stop]}},#{path.distance}>"
  end
end
