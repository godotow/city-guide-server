require 'map/trace'

class Map
  attr_reader :map
  def initialize map
    @map = map
  end
  def get point
    map.select { |x| x.start_at? point }
  end

  def get_map point
    Map.new get(point)
  end

  def traverse start, stop, limit
    results = []

    get(start).each do |trace|

      if trace.distance == limit && trace.stop == stop
        results << trace
        next
      end

      if trace.distance < limit
        if trace.stop == stop
          results << trace
          next
        end

        result = traverse(trace.stop, stop, limit - trace.distance)

        if Array(result).any?
          Array(trace).product(result).each { |x| results << x.flatten }
        end
      end
    end

    results
  end

  def find start, stop, limit
    traverse(start, stop, limit).map do |arry|
      if arry.is_a? Array
        first = arry.shift
        arry.each { |trace| first.add trace }
      else
        arry
      end
    end
  end
end
