class SimpleTrace
  attr_reader :start, :stop, :distance
  def initialize start, stop, distance
    @start, @stop, @distance = start, stop, distance
  end
  def to_s
    "#{self.class}:#{start}:#{stop}:#{distance}"
  end

  def ==(other)
    start == other.start && stop == other.stop && distance == other.distance
  end
end
