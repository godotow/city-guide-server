require 'spec_helper'
require './lib/map'

class TestMap < Map
  def initialize
    @map = []
    [
      [:a,:b,2],
      [:a,:c,3],
      [:a,:d,1],
      [:a,:e,7],

      [:b,:c,3],
      [:b,:d,7],
      [:b,:e,8],

      [:c,:d,4],
      [:c,:e,3],

      [:d,:e,3],
    ].each do |x|
      @map << Map::Trace.new(*x)
      @map << Map::Trace.new(x[1],x[0],x[2])
    end
  end
end

describe TestMap do
  subject(:map) { TestMap.new }
  it 'returns list of traces with :a included' do
    expect(map.get(:a).size).to eql 4
  end
  it 'returns list of traces with :b included' do
    expect(map.get(:b).size).to eql 4
  end

  it 'returns list of traces with :b included' do
    expect(map.get(:c).size).to eql 4
  end

  context 'trace finding' do
    it 'return path cf with 4 distance' do
      expect(
        map.find(:e, :a, 4).size
      ).to eql 1
    end
  end
end

describe Map::Trace do

  let(:trace_1) { described_class.new :c, :a, 2 }
  let(:trace_2) { described_class.new :a, :f, 2 }
  let(:trace_1_2) { trace_1.add trace_2 }


  it 'returns sum of sub traces' do

    expect(
      trace_1_2.distance
    ).to eql 4
  end
end


describe Map, 'Map#2' do

  let(:markers) {
    [
      [:a,:b,5],
      [:a,:c,4],
      [:b,:c,2],
      [:b,:d,3],
      [:c,:f,3],
      [:c,:e,1],
      [:c,:d,1],
      [:d,:e,1],
      [:d,:k,7],
      [:e,:f,3],
      [:e,:g,6],
      [:k,:g,6],
      [:k,:z,2]
    ].map do |x|
      [Map::Trace.new(*x), Map::Trace.new(x[1],x[0],x[2])]
    end.flatten
  }

  subject(:map) { Map.new markers }

  it 'returns traces' do
    expect(map.get(:a).count).to eql 2
    expect(map.get(:b).count).to eql 3
    expect(map.get(:c).count).to eql 5
    expect(map.get(:d).count).to eql 4
    expect(map.get(:e).count).to eql 4
    expect(map.get(:f).count).to eql 2
    expect(map.get(:g).count).to eql 2
    expect(map.get(:k).count).to eql 3
    expect(map.get(:z).count).to eql 1
  end

  it 'returns path fedk & fcdk' do
    expect(
      map.find(:f,:k,11).count
    ).to eql 2

    trace = Map::Trace.new :f,:c,3
    trace.add Map::Trace.new :c,:d,1
    trace.add Map::Trace.new :d,:k,7

    expect(
      map.find(:f,:k,11).first
    ).to eql trace

    trace = Map::Trace.new :f,:e,3
    trace.add Map::Trace.new :e,:d,1
    trace.add Map::Trace.new :d,:k,7

    expect(
      map.find(:f,:k,11).last
    ).to eql trace

  end
  it 'long distance' do
    expect(
      map.find(:f,:k,14)
    ).to eql []

  end

end
